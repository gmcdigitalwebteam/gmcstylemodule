# GMC Style Module

Shared global stylesheet to keep GMC UI consistent with the design system.

- Evolution of our npm global styling repo
- Can be used either as a git submodule (recommended) or as an npm module
- New stuff, might break things


## Installation

GMC Style module requires git to run. 

Please install it the folder where project assets live. For example: src/assets/

```sh
git submodule add https://rofi_gmc@bitbucket.org/gmcdigitalwebteam/gmcstylemodule.git
```
This will fetch the module into gmcstylemodule folder.

## Using it

This git submodule will sit inside a project just like any other folder so that any root/index scss file can reference, watch and debug everything inside the module. Despite being inside the project, because it's a submodule, the files inside the module will be tracked separatley.

If you're cloning a project that already uses this as a git module, you must initiate and update the module before starting.

For example, if you have cloned gmconlinev3fe project and want to initiate this submodule, do the following:
```sh
cd gmconlinev3fe
git submodule update --init --recursive
```
Note that the update command will fetch and update the module to the latest point where the root project had made a commit. Ensuring that even if the module repo gets updated, it won't break the project.

To update module to latest::
```sh
git submodule update --remote --merge
```

Note that even thought this will update the module to latest, to make sure the project itself is using the latest you must commit this change as part of the project git.

## Making changes to style module and raising PR
Changes to the module can be made right into the project without cloning the module repo itself. To do so, simply cd into the folder inside your project and then

- Checkout a new branch off master
- make changes 
- commit and push
- Raise a PR in gmcstylemodule repo
- Get it merged
- checkout to master branch
- run ```git fetch --all --tags --prune ```
- run ```git tag```
- Choose the tag/version that you want to use in the project
- run ```git checkout {tag}``` for example: 1.0.2-rc 
- commit this change as part of the project (not the submodule)
- raise PR to get project changes merged.

Note: Tags are created by Teamcity when latest gets merged to master

