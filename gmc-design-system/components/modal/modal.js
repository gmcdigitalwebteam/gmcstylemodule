﻿var lastFocused;

function showModal(modal) {
    $(modal).removeClass("u-hidden");
}

function closeModal(modal) {
    $(modal).addClass("u-hidden");
    lastFocused.focus();
}

function focusTrapOpenModal() {
    var modals = document.getElementsByClassName('c-modal');

    for (var i = 0; i < modals.length; i++) {
        //if modal does not contain the u-hidden class and is therefore active
        if (!modals[i].classList.contains('u-hidden')) {
            var modalID = "#" + modals[i].id
            var element = document.getElementById(modals[i].id)
            var modalHasFocus = (document.activeElement === element)

            //if the modal is not the active element AND it's children are not the active element, trap focus back inside the modal
            if (!modalHasFocus && !element.contains(document.activeElement)) {
                lastFocused = document.activeElement;
                focusTrap(modalID)
            }
        }
    }
}

var allModals = document.querySelectorAll(".c-modal");
for (var i = 0; i < allModals.length; i++) {
    //if modal is displayed when the page is rendered, trap focus within.
    if (!allModals[i].classList.contains('u-hidden')) {
        var modalID = "#" + allModals[i].id
        lastFocused = document.activeElement;
        focusTrap(modalID)
    }
    observeModal(allModals[i]);
}

function observeModal(modal) {
    // create an observer instance
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            var modalID = "#" + modal.id
            if (mutation.type === 'attributes') {
                if (modal.classList.contains('u-hidden')) {
                    modal.removeAttribute("aria-modal");
                    modal.setAttribute("aria-hidden", "true");
                }
                else {
                    lastFocused = document.activeElement;
                    modal.removeAttribute("aria-hidden");
                    modal.setAttribute("aria-modal", "true");
                    focusTrap(modalID)
                }
            }
        });
    });

    // configuration of the observer
    var config = {
        attributes: true,
        attributeFilter: ['class']
    };

    // pass in the target node, as well as the observer options
    observer.observe(modal, config);
}

//Make sure focus is back in the modal if it is active.
document.addEventListener('keydown', function (e) {
    if (e.keyCode === 9) {
        focusTrapOpenModal()
    }

    if (e.keyCode === 13) {
        if (event.target.getAttribute("id") === "skiptToContentLink") {
            focusTrapOpenModal()
        }
    }
});
