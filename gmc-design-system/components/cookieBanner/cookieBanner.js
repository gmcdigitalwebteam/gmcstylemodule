﻿function cookieBanner() {
    var $page = {
        isCookiePreferencesPage: $(".c-cookie-preferences").length > 0,
        isPrintView: $("body").hasClass("print-view")
    };
    
    var $elements = {
        cookieBanner: $(".c-cookie-banner"),
        cookieButton: $(".c-cookie-banner .c-button")
    };

    var $attributes = {
        newPolicy: $elements.cookieBanner.data("new-policy"),
        preferencesSetCookie: $elements.cookieBanner.data("preferences-set-cookie"),
        policyCookie: $elements.cookieBanner.data("policy-cookie"),
        expiryPeriod: $elements.cookieBanner.data("expiry-period")
    };

    this.init = function () {
        if ($attributes.newPolicy) {
            if (!$page.isPrintView && !$page.isCookiePreferencesPage) {
                $elements.cookieBanner.addClass("c-cookie-banner--visible");
            }
        }
        else if (!$attributes.newPolicy && !$page.isPrintView) {
            $elements.cookieBanner.addClass("c-cookie-banner--visible");
        }

        $elements.cookieButton.on("click",
            function () {
                if ($attributes.newPolicy) {
                    acceptAllCookies($attributes.policyCookie, $attributes.expiryPeriod);

                }   
                $elements.cookieBanner.removeClass("c-cookie-banner--visible");
            }
        );
    };
}