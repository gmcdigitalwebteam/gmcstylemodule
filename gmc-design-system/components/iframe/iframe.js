﻿//Get the iframe id
var getiFrameID = "#" + $('.c-iframe').attr('id');
var iFrameID = document.querySelector(getiFrameID)

//Take the iframes inner body scroll height and apply it to the iframe itself
function toggleIframeHeight() {
    iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
}