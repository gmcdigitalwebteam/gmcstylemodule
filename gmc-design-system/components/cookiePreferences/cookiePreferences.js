﻿function cookiePreferences() {
    var $elements = {
        cookiePreferences: $(".c-cookie-preferences"),
        acceptAllButton: $(".js-cookie-preferences-accept-all-button"),
        acceptAllDialogueMessage: $(".js-cookie-preferences-accept-all-dialogue-message"),
        toggles: $(".js-cookie-preferences-toggle"),
        toggleSwitch: $(".js-cookie-preferences-toggle-switch .c-toggle-switch__inner-container"),
        saveSettingsButton: $(".js-cookie-preferences-save-settings-button"),
        saveSettingsDialogueMessage: $(".js-cookie-preferences-save-settings-dialogue-message")
    };

    var $attributes = {
        preferencesSetCookie: $elements.cookiePreferences.data("preferences-set-cookie"),
        policyCookie: $elements.cookiePreferences.data("policy-cookie"),
        expiryPeriod: $elements.cookiePreferences.data("expiry-period")
    };

    this.init = function() {
        $elements.acceptAllButton.on("click", function() {
                acceptAllCookies($attributes.policyCookie, $attributes.expiryPeriod);
                cookiePreferences.updateTogglesFromPolicyCookie();
                $elements.acceptAllButton.parent().addClass("u-hidden");
                $elements.acceptAllDialogueMessage.removeClass("u-hidden");
                cookiePreferences.setPreferencesSetCookie(false);
            });
        };

    this.updateTogglesFromPolicyCookie = function () {
        if (Cookies.get($attributes.policyCookie)) {
            $elements.toggles.each(function () {
                var cookieCategory = $(this).find($elements.toggleSwitch).prop('htmlFor');
                var cookieCategoryPreferences = getCookieCategoryPreferences($attributes.policyCookie);
                var categoryEnabled = cookieCategoryPreferences[cookieCategory];

                var hiddenCheckbox = $("#" + cookieCategory);
                var toggleSwitchOn = hiddenCheckbox.attr('aria-checked') === 'true';

                // Only change the toggle switch state if its current state does not match the
                // associated cookie category state
                if (categoryEnabled !== toggleSwitchOn) {
                    changeToggleSwitchState(hiddenCheckbox);
                    hiddenCheckbox.click();
                }
            });
        }
    };

    this.updatePolicyCookieFromToggles = function () {
        if (Cookies.get($attributes.policyCookie)) {
            var cookieCategoryPreferences = getCookieCategoryPreferences($attributes.policyCookie);
            if (cookieCategoryPreferences) {
                $elements.toggles.each(function () {
                    var cookieCategory = $(this).find($elements.toggleSwitch).prop('htmlFor');
                    var hiddenCheckbox = $("#" + cookieCategory);
                    var toggleSwitchOn = hiddenCheckbox.attr('aria-checked') === 'true';
                    cookieCategoryPreferences[cookieCategory] = toggleSwitchOn ? true : false;
                });

                Cookies.set($attributes.policyCookie, cookieCategoryPreferences,
                    {
                        expires: $attributes.expiryPeriod,
                        path: "/",
                        secure: true
                    });
            }
        }
    };

    this.setPreferencesSetCookie = function (expireCookies) {
        if (Cookies.get($attributes.policyCookie)) {
            Cookies.set($attributes.preferencesSetCookie, { date: new Date().toISOString(), expireCookies: expireCookies },
                {
                    expires: $attributes.expiryPeriod,
                    path: "/",
                    secure: true
                });
        }
    }

    $elements.saveSettingsButton.on("click", function () {
        cookiePreferences.updatePolicyCookieFromToggles();
        $elements.saveSettingsButton.parent().addClass("u-hidden");
        $elements.saveSettingsDialogueMessage.removeClass("u-hidden");
        cookiePreferences.setPreferencesSetCookie(true);
    });

    $elements.toggles.on('click', function () {
        if ($elements.acceptAllButton.parent().hasClass("u-hidden")) {
            $elements.acceptAllButton.parent().removeClass("u-hidden");
            $elements.acceptAllDialogueMessage.addClass("u-hidden");
        }

        if ($elements.saveSettingsButton.parent().hasClass("u-hidden")) {
            $elements.saveSettingsButton.parent().removeClass("u-hidden");
            $elements.saveSettingsDialogueMessage.addClass("u-hidden");
        }
    });
}
