﻿$(document).ready(function () {
    // select the header profile section by its ID
    var userProfileMenuFocusTrappedElement = document.querySelector('#userProfileMenu'); 

    // header profile section is only present on page if user is signed in
    if (!userProfileMenuFocusTrappedElement) {
        return;
    }
    //Variables used for trapping focus within the popup itself
    var userProfileMenuFocusableElements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
    var userProfileMenuFirstFocusableElement = userProfileMenuFocusTrappedElement.querySelectorAll(userProfileMenuFocusableElements)[1]; // get the first element to be focused inside the header profile section
    var userProfileMenuFocusableContent = userProfileMenuFocusTrappedElement.querySelectorAll(userProfileMenuFocusableElements);
    var userProfileMenuNextFocusableElement = userProfileMenuFocusTrappedElement.querySelectorAll(userProfileMenuFocusableElements)[2]; // get the next element to be focused inside the header profile section
    var userProfileMenuLastFocusableElement = userProfileMenuFocusableContent[userProfileMenuFocusableContent.length - 1]; // get the last element to be focused inside the header profile section

    function toggleProfilePopup() {
        var profileAriaExpanded = "false"
        $("#profilePopup").toggleClass("c-user-profile-menu-popup--hide");
        $("#profileUserIcon").toggleClass("c-user-profile-menu__icon-user c-user-profile-menu__icon-close");
        $("#userProfileMenu").toggleClass("js-user-profile-menu-active");
        $("#userProfileMenuTransparentOverlay").toggleClass("c-user-profile-menu__transparent-overlay--show");
        if ($("#profileUserIcon").hasClass("c-user-profile-menu__icon-close")) {
            profileAriaExpanded = "true"
        }
        $("#profileUserIcon").attr("aria-expanded", profileAriaExpanded)
    }

    function scrollTopForPopup() {
        var currentScrollPosition = $('html').scrollTop();
        //Only scroll to top if we are not at the top of the page
        if (currentScrollPosition > 0) {
            $("html, body").animate({ scrollTop: 0 }, 100)
                //Animating two elements so we get two callbacks but only need to execute the function once. 
                //Promise method used to get single callback of multiple element animations
                .promise().then(function () {
                    //Toggle the popup ONCE when animation is complete
                    toggleProfilePopup()
                });
        }
        //else just toggle the popup immediately instead of waiting for scroll animation to complete
        else {
            toggleProfilePopup()
        }
    }

    //Close the header profile popup when clicking outside of the profile popup container 
    $(document).on({
        'click': function (e) {
            if (e.target.id != 'profilePopup' && e.target.id != 'profileUserIcon' && !$('#profilePopup').find(e.target).length && !$("#profilePopup").hasClass("c-user-profile-menu-popup--hide")) {
                if ($(window).width() >= breakpointLarge) {
                    toggleProfilePopup();
                }
            }
        }
    })

    //Close the header profile popup when resizing the window to below the large breakpoint
    $(window).on('resize', function () {
        if ($(window).width() < breakpointLarge) {
            if (!$("#profilePopup").hasClass("c-user-profile-menu-popup--hide")) {
                toggleProfilePopup()
            }
        }
    });

    //Close the header profile pop-up if it is open when scrolling
    $(window).on('scroll', function () {
        //Get the current scroll postion value of the page
        var currentScrollPosition = $('html').scrollTop();

        if (!$("#profilePopup").hasClass("c-user-profile-menu-popup--hide") && currentScrollPosition > 0) {
            toggleProfilePopup()
        }
    });

    $('#profileUserIcon').on({
        //Clicking on the profile user icon 
        'click': function (e) {
            e.preventDefault()
            scrollTopForPopup()
        },
        'keydown': function (e) {
            //Pressing the tab key on the profile user icon
            if (e.keyCode === 9) {
                //If the tab key and shift key are pressed together
                if (e.shiftKey) {
                    if ($("#userProfileMenu").hasClass("js-user-profile-menu-active")) {
                        userProfileMenuLastFocusableElement.focus()
                        e.preventDefault();
                    }
                }
                //Else if it is just the tab key being pressed
                else {
                    if ($("#userProfileMenu").hasClass("js-user-profile-menu-active")) {
                        userProfileMenuNextFocusableElement.focus()
                        e.preventDefault();
                    }
                }
            }

            //Pressing the escape key will close the header profile popup
            if (e.keyCode === 27) {
                if ($("#userProfileMenu").hasClass("js-user-profile-menu-active")) {
                    toggleProfilePopup()
                }
            }
        }
    })

    $('#profilePopup a').on({
        'keydown': function (e) {
            //Pressing tab on an anchor tag within the header profile popup
            if (e.keyCode === 9) {
                if (e.shiftKey) {
                    if (document.activeElement === userProfileMenuNextFocusableElement) {
                        userProfileMenuFirstFocusableElement.focus()
                        e.preventDefault();
                    }
                }
                else {
                    if (document.activeElement === userProfileMenuLastFocusableElement) {
                        userProfileMenuFirstFocusableElement.focus()
                        e.preventDefault();
                    }
                }
            }

            //Pressing the escape key when focusing on anchor tag within the profile popup will close the popup and switch focus back to the profile icon
            if (e.keyCode === 27) { 
                toggleProfilePopup()
                userProfileMenuFirstFocusableElement.focus()
                e.preventDefault();
            }
        }
    })
});