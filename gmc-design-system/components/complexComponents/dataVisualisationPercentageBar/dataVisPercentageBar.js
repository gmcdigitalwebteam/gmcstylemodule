﻿'use strict';

// Animate percentage bar with the class c-data-vis-percentage-bar__indicator by adding the c-data-vis-percentage-bar__indicator--animate 
// when it is scrolled in to view.
toggleScrolledAnimation('.c-data-vis-percentage-bar__indicator', 'c-data-vis-percentage-bar__indicator--animate')