﻿'use strict';
// Script to set the data visualisation component segments values to values from the data-val attribute
toggleScrolledAnimation('.c-data-vis-doughnut', 'c-data-vis-doughnut--animate');

// Return an array list of doughnut components
const doughnutList = document.querySelectorAll('.c-data-vis-doughnut__segment')

// Execute a provided function once for each doughnut element in the array
Array.prototype.forEach.call(doughnutList, doughnut => {
    // Retrieve the value from the doughnut filled segments data-value attribute
    let doughnutValue = doughnut.dataset.val

    // Calculate the value for the white ring
    let doughnutRing = (100 - doughnutValue)
    let counter = 0;

    // Keep increasing the doughnut strok-dasharry values until the counter value equals the doughnut segment value
    let interval = setInterval(function () {
        if (counter === doughnutValue) {
            //Stop the setInterval method from calling the function
            window.clearInterval(interval);
        }
        doughnut.setAttribute("stroke-dasharray", doughnutValue + ", " + doughnutRing)
        counter++;
    }, 3);
})