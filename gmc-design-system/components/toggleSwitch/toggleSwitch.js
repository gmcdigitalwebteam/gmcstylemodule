﻿function turnToggleSwitchOn(element) {
    $(element).attr('aria-checked', 'true');
    $(element).nextAll().find('.c-toggle-switch__on-short-description').removeClass('u-hidden');
    $(element).nextAll().find('.c-toggle-switch__on-long-description').removeClass('u-hidden');
    $(element).nextAll().find('.js-toggle-switch__off-short-description').addClass('u-hidden');
    $(element).nextAll().find('.js-toggle-switch__off-long-description').addClass('u-hidden');
}

function turnToggleSwitchOff(element) {
    $(element).attr('aria-checked', 'false');
    $(element).nextAll().find('.js-toggle-switch__off-short-description').removeClass('u-hidden');
    $(element).nextAll().find('.js-toggle-switch__off-long-description').removeClass('u-hidden');
    $(element).nextAll().find('.c-toggle-switch__on-short-description').addClass('u-hidden');
    $(element).nextAll().find('.c-toggle-switch__on-long-description').addClass('u-hidden');
}

function changeToggleSwitchState(element) {
    if (element.attr('aria-checked') === 'true') {
        turnToggleSwitchOff(element);
    } else {
        turnToggleSwitchOn(element);
    }
}

$(document).ready(function () {
    // Prevent toggle switching when clicking on the label text
    $('.c-toggle-switch__text-container').on('click', function (e) {
        e.preventDefault();
    });

    // Targetting click events
    $('.c-toggle-switch__slider').on('click', function () {
        var hiddenCheckbox = $(this).parent().prev('.c-toggle-switch__hidden-checkbox');
        changeToggleSwitchState(hiddenCheckbox);
    });

    // Targetting keypress events on the toggle switch slider
    $('.c-toggle-switch__hidden-checkbox').on('keypress', function (e) {
        // If the enter key has been pressed
        if (event.which === 13) {
            var hiddenCheckbox = $(this);

            // Change the checked attribute when a the enter key is used to interact with the toggle switch
            hiddenCheckbox.prop('checked', !hiddenCheckbox.prop('checked'));

            changeToggleSwitchState(hiddenCheckbox);
        }
    });
});
