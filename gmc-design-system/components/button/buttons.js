﻿$(function () {
    $('.c-button--disabled').each(function (index, item) {
        $(item).attr("disabled", true);
    })
});

$('button').mouseup(function () {
    $(this).blur();
})

//Print button functionality
$('.js-print').click(function (e) {
    e.preventDefault();

    var el = $(this);
    var url = el.attr("href");



    el.before('<div class="iframe-wrapper"><iframe style="" id="printView" src="' + url + '" name="frame" style="" class="print-iframe"></iframe></div>');

    var $iFrame = $("#printView");
    $($iFrame).on('load',
        function () {
            setTimeout(function () {
                //Condition to make sure print fires in IE
                if (!!navigator.userAgent.match(/Trident\/7\./)) {
                    $iFrame[0].focus;
                    $iFrame[0].contentWindow.document.execCommand('print', false, null);
                    $(".iframe-wrapper").remove();
                } else {
                    $iFrame.focus;
                    $iFrame.get(0).contentWindow.print();
                    $(".iframe-wrapper").remove();
                }
            },
                1000);
        });
});
