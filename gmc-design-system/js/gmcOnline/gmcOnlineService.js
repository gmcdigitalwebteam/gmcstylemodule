﻿var gmcOnlineService = gmcOnlineService || {};

var gmcOnlineService = {

    $iframe: null,
    userNavigation: null,

    isLoggedIn: function () {
        return gmcOnlineService.$iframe != null &&
            gmcOnlineService.$iframe.contentWindow.SiebelApp !== undefined &&
            typeof gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetActiveView === "function" ? true : false;
    },
    
    setActiveView: function (viewName) {
        if (gmcOnlineService.isLoggedIn()) {
            gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GotoView(viewName);
        }
    },
    
    getActiveView: function () {
        if (!gmcOnlineService.isLoggedIn() ||
            typeof gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetActiveView().GetName !== "function") return null;

        return gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetActiveView().GetName();
    },

    getSource: function() {
        return gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetService("GMC Online Host");
    },

    //method to call the Siebel Menu API via the SiebelApp object
    //the type parameter is optional. If a type is passed then only the menu items for that type are returned
    getUserNavigation: function (type) {
        var siebelApi = gmcOnlineService.getSource();

        if (type === undefined) type = "";

        var request = gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.NewPropertySet();
        request.SetProperty("Username", gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetUserName());
        request.SetProperty("Type", type);

        var response = siebelApi.InvokeMethod("UserNavigation", request);

        return response.GetChildByType("ResultSet");
    },
    showMenu: function () {
        gmcOnlineService.userNavigation = gmcOnlineService.getUserNavigation();
        var homePage = gmcOnlineService.getHomePage();
        return homePage.propArray["ShowMenu"] === "Y" ? true : false;
    },

    getMenu: function () {
        return gmcOnlineService.userNavigation.GetChildByType("Menu");
    },

    getHomePage: function () {
        return gmcOnlineService.userNavigation.GetChildByType("Home Page");
    },

    getHomePageDefaultView: function () {
        var homePage = gmcOnlineService.getHomePage();
        return homePage.propArray["DefaultView"];
    },

    getHomePageView: function () {
        var homePage = gmcOnlineService.getHomePage();
        return homePage.propArray["HomePage"];
    },

    GetProfileAttr: function (attribute) {
        return gmcOnlineService.$iframe.contentWindow.SiebelApp.S_App.GetProfileAttr(attribute);
    }
}