﻿var win = $(this); //this = window
var dashboardLeftColumn = "#gmcOnlineLeftColumn";
var dashboardRightColumn = "#gmcOnlineRightColumn";
var dashboardSideMenu = "#gmcOnlineSideNavigation";
var dashboardToggleButton = "#gmcOnlineDashboardToggleMenuButton";
var dashboardToggleButtonIcon = "#gmcOnlineDashboardToggleMenuButtonIcon";
var dashboardToggleButtonText = "#gmcOnlineDashboardToggleMenuButtonText";

function internetExplorerRefreshed() {
    return window.performance && window.performance.navigation.type === 1 && isInternetExplorer();
}

function isInternetExplorer() {
    return window.document.documentMode ? true : false;
}

function refreshMenu() {
    gmcOnlineService.userNavigation = gmcOnlineService.getUserNavigation();
    showApplicableSecondaryItems();
}

function reloadPage() {
    //reload page to keep Siebel session active
    window.location.reload();
    localStorage.setItem("pageReloaded", "true");
}

function showApplicableSecondaryItems() {
    var menu = gmcOnlineService.getMenu();

    menu.childArray.forEach(function (menuSection) {

        var $navItems = $(sideMenuItem).find("[data-menu-type='" + menuSection.type + "']").toArray();

        $navItems.forEach(function (secondaryItem) {
            if ($(secondaryItem).attr("data-menu-key") !== undefined) {
                if (menuSection.propArray[$(secondaryItem).attr("data-menu-key")]) {
                    $(secondaryItem).attr('data-menu-view', menuSection.propArray[$(secondaryItem).attr("data-menu-key")]);
                    $(secondaryItem).parent().removeClass('u-hidden');
                }
                else {
                    $(secondaryItem).attr('data-menu-view',"");
                    $(secondaryItem).parent().addClass('u-hidden');
                }
            }
        });
    });
}

function onBrowserNavigationButtonClicked() {
    var forwardBackButtonClicked = window.performance && window.performance.navigation.type === 2;

    if (forwardBackButtonClicked) {
        reloadPage();
    }
}

function toggleFullWidthIframe() {
    $("#gmcOnlineDashboard").toggleClass(" u-top-margin-5 u-top-margin-9");
    $(dashboardLeftColumn).toggleClass("js-gmc-online-left-column-hidden");
    $(dashboardLeftColumn).toggle();
    $(dashboardRightColumn).toggleClass('o-column-9 o-column-12');
}

function toggleSideNavigationVisibility() {
    $(dashboardSideMenu).toggleClass('u-hidden');
    $(dashboardToggleButton).toggleClass("u-hidden");
}

function togglePrimaryNavVisibility() {
    $('#primary-nav').children().toggle();
}

function toggleDashboardMenu(forceHide) {
    if (forceHide) {
        $(dashboardSideMenu).addClass("t-width-0");
        $(dashboardSideMenu).addClass("u-visibility-hidden");
        $(dashboardRightColumn).addClass("o-column-dashboard-100");
        $(dashboardLeftColumn).addClass("o-column-dashboard-0");
        $(dashboardToggleButtonText).text("Open menu");
        $(dashboardToggleButtonIcon).removeClass("fa-outdent");
        $(dashboardToggleButtonIcon).addClass("fa-indent");
    }
    else {
        $(dashboardSideMenu).toggleClass("t-width-0");
        $(dashboardSideMenu).toggleClass("u-visibility-hidden");
        $(dashboardRightColumn).toggleClass("o-column-dashboard-100");
        $(dashboardLeftColumn).toggleClass("o-column-dashboard-0");
        var text = $(dashboardToggleButtonText).text();
        $(dashboardToggleButtonText).text(text == "Close menu" ? "Open menu" : "Close menu")
        $(dashboardToggleButtonIcon).toggleClass("fa-outdent fa-indent");

        setTimeout(function () {
            toggleIframeHeight()
        }, 250)
    }
}

function showDefaultLayout() {
    if ($(dashboardToggleButton).hasClass("u-hidden")) {
        toggleSideNavigationVisibility();
    }

    if (!$('#primary-nav .nav-menu').is(':visible')) {
        togglePrimaryNavVisibility();
    }

    if ($(dashboardRightColumn).hasClass("o-column-12")) {
        toggleFullWidthIframe();
    }
}

function toggleGmcDashboardLoadingOverlay() {
    $("#gmcOnlineDashboardLoadingOverlay").toggleClass("c-loading-overlay--hide");
}

function syncMenuToView(activeView) {

    if (activeView === null || $(dashboardToggleButton).hasClass("u-hidden")) return;

    //Remove the keyword 'Amend' when getting the menu item for the view. Each readonly view has an equivalent amend view but in our menu we only reference the readonly views 
    //and a link within a view may link straight to an editable view so removing it ensures we find the correct menu item
    var menuItemForView = $(dashboardSideMenu).find("[data-menu-view='" + activeView.replace("Amend ", "") + "']").first();

    if (menuItemForView.length > 0 && menuItemForView.attr('aria-current') !== 'page') {

        var targetId = $(menuItemForView).data('target');

        if (targetId) {
            $("li[id='" + targetId + "'] ul").show();

            selectDropdownLink(menuItemForView);
        } else {
            selectNavLink(menuItemForView);
        }
    }
}

var gmcOnlineTimeout;

function SessionWarn(expiryTime) {
    showModal("#gmcOnlineTimeoutWarning")

    gmcOnlineTimeout = window.setTimeout(function () {
        window.location.href = "/account-settings/auto-sign-out";
    }, expiryTime);
}

function staySignedIn() {
    window.clearTimeout(gmcOnlineTimeout);
    // We send a test request to Siebel to keep the session alive without interpreting the experience
    gmcOnlineService.GetProfileAttr("test");
    closeModal("#gmcOnlineTimeoutWarning");
}

$(document).ready(function () {
    if ($("#gmcOnlineDashboard").length > 0) {
        
        var settings =
        {
            iframeName: "gmcOnlineIframe",
            $navigation: $(dashboardSideMenu),
            loadNavigation: true,
            activeView: ""
        }

        $(window).on('pageshow', function () {
            onBrowserNavigationButtonClicked();
        });

        $("#" + settings.iframeName).ready(function () {
            var gmcIframe = gmcOnlineService.$iframe;

            var iframeSrcObserver = new MutationObserver(function (mutations) {
                mutations.forEach(function (mutation) {
                    if (mutation.type == "attributes" && mutation.attributeName == "src") {
                        var src = mutation.target.getAttribute("src");
                        var showOverlay = src.includes("/system-updates") || src.includes("/system-maintenance");

                        if (showOverlay) {
                            var overlay = window.parent.$("#gmcOnlineDashboardLoadingOverlay");
                            if (overlay.hasClass('c-loading-overlay--hide')) {
                                toggleGmcDashboardLoadingOverlay();
                            }
                        }
                    }
                });
            });
            iframeSrcObserver.observe($(gmcIframe)[0], {
                attributes: true
            });

            //wait for the login view to load
            $(gmcIframe).on("load", function () {
                // Handle display of the loading overlay on the  local dev login page
                var siebelLoginButton = $("#" + settings.iframeName).contents().find('#s_swepi_22');
                if (siebelLoginButton.length != 0) {
                    toggleIframeHeight()
                    toggleGmcDashboardLoadingOverlay()
                }
                // Targeting click on the siebel login button - for local development only
                $(siebelLoginButton).click(function () {
                    toggleGmcDashboardLoadingOverlay()
                });
            });

            if (localStorage.getItem("pageReloaded") === "true" || internetExplorerRefreshed()) {
                document.getElementById(settings.iframeName).contentWindow.location.reload();
            }

            loadContent(settings);

            $(window).on('resize', function () {
                toggleIframeHeight()
            })
        });

        function loadContent(settings) {
            //this monitors any changes to the content of the iframe so that we can check if we've successfully logged in and call the menu api
            var observer = new MutationObserver(function (mutations) {
                //MutationObserver is a little too observant and wants to capture every event! I've reigned it in so that we're only concerned when a view loads
                if (mutations.length > 0 && (mutations[0].target.className.includes("siebui") || mutations[0].target.classList.contains("PortletMode"))) {
                    if (gmcOnlineService.isLoggedIn() && settings.loadNavigation) {
                        //we only want to load the full navigation once on login
                        settings.loadNavigation = false;
                        renderNavigation();

                        var selectedView = localStorage.getItem("activeView") ? localStorage.getItem("activeView") : "";

                        initView(gmcOnlineService.getHomePageDefaultView(), selectedView);

                        settings.activeView = gmcOnlineService.getActiveView();
                        onViewLoaded(toggleGmcDashboardLoadingOverlay);
                    }

                    if (settings.activeView !== gmcOnlineService.getActiveView()) {
                        settings.activeView = gmcOnlineService.getActiveView();
                        //check if we need to show the menu before syncing it to the view
                        if ($(dashboardToggleButton).hasClass("u-hidden")) {
                            renderNavigation();
                        }
                        syncMenuToView(settings.activeView);
                        onViewLoaded(toggleIframeHeight);
                    }
                }
            });

            gmcOnlineService.$iframe.onload = function () {
                observer.observe(gmcOnlineService.$iframe.contentWindow.document.body, {
                    childList: true,
                    subtree: true
                });
            };
        }

        function onViewLoaded(callback) {
            if ($("#" + settings.iframeName).contents().find(".siebui-mask-overlay").is(":hidden")) {
                callback();
            } else {
                setTimeout(function () {
                    onViewLoaded(callback);
                }, 1000);
            }
        }

        function setHomeButtonView() {
            var $homeButton = $(dashboardSideMenu).find("[data-menu-type='Home Page']")
                .filter('[data-menu-key="HomePage"]');
            $homeButton.attr('data-menu-view', gmcOnlineService.getHomePageView());
        }

        function initView(homePageDefaultView, selectedView) {
            if (localStorage.getItem("pageReloaded") === "true") {
                gmcOnlineService.setActiveView(selectedView);
                syncMenuToView(selectedView);
            } else {
                gmcOnlineService.setActiveView(homePageDefaultView);
                localStorage.setItem("activeView", homePageDefaultView);
            }

            localStorage.removeItem("pageReloaded");
        }

        var updateViewQueue = [];

        function queueUpdateView(view) {

            updateViewQueue.push(view);
            processUpdateViewQueue();
        }

        function processUpdateViewQueue() {
            setTimeout(function () {
                if ($("#" + settings.iframeName).contents().find(".siebui-mask-overlay").is(":hidden")) {
                    var view = updateViewQueue.pop(); //get the last item in the queue
                    updateViewQueue = []; //reset the queue
                    if (view !== undefined && view !== gmcOnlineService.getActiveView()) {
                        gmcOnlineService.setActiveView(view);
                        localStorage.setItem("activeView", view);
                        syncMenuToView(view);
                    }
                }
                //check if queue has been updated since processing last request
                if (updateViewQueue.length > 0) {
                    processUpdateViewQueue();
                }

            }, 1000);
        }

        function updateView(event) {
            if ($("#" + settings.iframeName).contents().find(".siebui-mask-overlay").is(":hidden") &&
                updateViewQueue.length === 0) {
                var viewName = event.target.dataset.menuView;
                gmcOnlineService.setActiveView(viewName);
                localStorage.setItem("activeView", viewName);
            }
            else {
                queueUpdateView(event.target.dataset.menuView);
            }
        }

        function renderNavigation() {
            $(sideMenuItem).each(function () {
                removeLastVisibleItemBorder($(this).find(dropdownItem));
            });

            if (gmcOnlineService.showMenu()) {
                setHomeButtonView();
                showApplicableSecondaryItems();
                showDefaultLayout();
                bindEventListeners();
            } else {
                if ($('#primary-nav .nav-menu').is(':visible')) {
                    toggleFullWidthIframe();
                    togglePrimaryNavVisibility();
                }
            }
        }

        function bindEventListeners() {
            $("#gmcOnlineSideNavigation a[data-menu-view]").on("click", function (e) {
                e.preventDefault();
                if (gmcOnlineService.isLoggedIn()) {
                    updateView(e);
                }
                else {
                    reloadPage();
                }
            });
            $("#gmcOnlineSideNavigation a[data-menu-view]").on("keydown", function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    if (gmcOnlineService.isLoggedIn()) {
                        updateView(e);
                    } else {
                        reloadPage();
                    }
                }
            });
            $("#gmcOnlineStaySignedIn").on("click", function (e) {
                e.preventDefault();
                if (gmcOnlineService.isLoggedIn()) {
                    staySignedIn();
                }
            });
            $("#gmcOnlineStaySignedIn").on("keydown", function (e) {
                if (e.which === 13) {
                    e.preventDefault();
                    if (gmcOnlineService.isLoggedIn()) {
                        staySignedIn();
                    }
                }
            });
        }

        (function init() {
            gmcOnlineService.$iframe = document.querySelector("#" + settings.iframeName);
        })();

        // Targeting click events on the toggle menu button
        $(dashboardToggleButton).click(function () {
            toggleDashboardMenu()
        });

        $(dashboardToggleButton).on('keydown',
            function (e) {
                if (e.which == 13) {
                    e.preventDefault()
                    toggleDashboardMenu()
                }
            });

        $(window).on('resize', function () {
            if (win.width() < breakpointMedium) {
                if (!$(dashboardLeftColumn).hasClass("js-gmc-online-left-column-hidden")) {
                    if ($(dashboardSideMenu).hasClass("t-width-0")) {
                        toggleDashboardMenu()
                    }
                }
            }
        });
    };
});