﻿function acceptAllCookies(policyCookieName, expiryPeriod) {
    var cookiePreferences = getCookieCategoryPreferences(policyCookieName);

    if (cookiePreferences) {
        for (var cookieCategory in cookiePreferences) {
            cookiePreferences[cookieCategory] = true;
        }

        Cookies.set(policyCookieName, cookiePreferences,
            {
                expires: expiryPeriod,
                path: "/",
                secure: true
            });

        dataLayer.push({
            'event': 'CookiePreferences_AcceptAllCookies'
        });
    }
}

function getCookieCategoryPreferences(policyCookieName) {
    var policyCookieValue = Cookies.get(policyCookieName);
    if (!policyCookieValue) {
        return null;
    }
    return JSON.parse(decodeURIComponent(policyCookieValue));
}