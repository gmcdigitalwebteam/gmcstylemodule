﻿'use strict';

// Animate any given element(s) when scrolled in to view
function toggleScrolledAnimation(element, animation) {
    // ID/Class selector of the element(s) we want to animate
    const targetElement = document.querySelectorAll(element);

    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            // If the element is completely in view
            if (entry.intersectionRatio > 0) {
                // Add the animation class
                entry.target.classList.add(animation);
            } else {
                //Remove the animation class if the element is not in view
                entry.target.classList.remove(animation);
            }
        });
    });

    targetElement.forEach(target => {
        observer.observe(target);
    });
}
