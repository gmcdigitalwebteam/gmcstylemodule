﻿// Used to remove the ability to tab to a div added by LivePerson which can't be interacted with

$(document).ready(function () {
    $('.js-webchat-remove-tab-index').on("DOMSubtreeModified", function () {
        $(this).children().first().removeAttr("tabindex");
    });
});