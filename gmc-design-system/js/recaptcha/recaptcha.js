﻿function hideNonDisplayedRecaptchaElementsFromAccessibilityCheckingTools() {
    setTimeout(function () {
        var $textarea = $('#g-recaptcha-response');
        $textarea.attr('aria-hidden', true);
        $textarea.attr('aria-label', 'do not use');
        $textarea.attr('aria-readonly', true);

        var $nonDisplayedIframe = $('.g-recaptcha').find('iframe[style*="display: none"]');
        $nonDisplayedIframe.attr('aria-hidden', true);
        $nonDisplayedIframe.attr('aria-label', 'do not use');
        $nonDisplayedIframe.attr('aria-readonly', true);
    }, 2000);
}

function onloadRecaptcha() {
    hideNonDisplayedRecaptchaElementsFromAccessibilityCheckingTools();
}
