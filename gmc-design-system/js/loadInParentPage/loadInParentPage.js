﻿$(document).ready(function () {
    if ($("#jsLoadInParentPage").length > 0) {
        if (window.self !== window.top) {
            // This delay allows for code on the parent page to run before the redirect takes place
            setTimeout(function () {
                window.top.location.href = window.self.location.href;
            }, 2000);
        }
    }
});