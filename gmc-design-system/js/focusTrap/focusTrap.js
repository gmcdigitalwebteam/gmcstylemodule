﻿// Script to trap focus within an element, ideally for modals/pop-up windows such as the contact panel.
// focusTrapID = Id of the element you want to trap focus within

function focusTrap(focusTrapID) {
    var focusableElements = 'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])';
    var focusTrappedElement = document.querySelector(focusTrapID); // select the element by it's ID
    var firstFocusableElement = focusTrappedElement.querySelectorAll(focusableElements)[0]; // get the first element to be focused inside the element
    var focusableContent = focusTrappedElement.querySelectorAll(focusableElements);
    var lastFocusableElement = focusableContent[focusableContent.length - 1]; // get the last element to be focused inside the element
    $(focusTrapID).focus()

    document.addEventListener('keydown', function (e) {
        var isTabPressed = e.key === 'Tab' || e.keyCode === 9;
        if (!isTabPressed) {
            return;
        }

        if (e.shiftKey) { // if shift is key pressed for shift + tab combination (which tabs through the elements in reverse)
            if (e.target === focusTrappedElement || document.activeElement === firstFocusableElement) {
                lastFocusableElement.focus(); // focus on the last focusable element
                e.preventDefault();
            }
        }
        else { // if tab key is pressed
            if (document.activeElement === lastFocusableElement) { // if the last focusable element is in focus, switch focus to the first focusable element after pressing tab
                firstFocusableElement.focus(); // focus on the first focusable element
                e.preventDefault();
            }
        }
    });
}