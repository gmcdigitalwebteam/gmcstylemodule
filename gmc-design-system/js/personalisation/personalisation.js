﻿// Script to support Sitecore personalisation

function TriggerGoal(goalId) {
    var token = $('#AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();

    $.ajax({
        url: '/api/sitecore/personalisation/triggergoal',
        data: {
            "__RequestVerificationToken": token,
            'goalId': goalId
        },
        method: 'POST',
        error: function() {
            console.log('Failed to trigger goal with ID: ' + goalId);
        },
        datatype: "json"
    });
}