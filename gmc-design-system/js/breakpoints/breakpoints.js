﻿// Global breakpoint variables
// Using breakpoint values from Gmc.Static\assets\styles\vendor\_mq.scss

const breakpointXSmall = 0;
const breakpointSmall = 576;
const breakpointMedium = 767;
const breakpointLarge = 1024;
const breakpointXLarge = 1200;
const breakpointXXLarge = 1440;